package nl.robfaber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.Routes;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableDiscoveryClient
@SpringBootApplication
public class ApiGateway {
  public static void main(String[] args) {
    SpringApplication.run(ApiGateway.class, args);
  }

//  @Bean
//  public RouteLocator customRouteLocator(ThrottleWebFilterFactory throttle) {
//    return Routes.locator()
//        .route("test")
//        .uri("http://httpbin.org:80")
//        .predicate(host("**.abc.org").and(path("/image/png")))
//        .addResponseHeader("X-TestHeader", "foobar")
//        .and()
//        .route("test2")
//        .uri("http://httpbin.org:80")
//        .predicate(path("/image/webp"))
//        .add(addResponseHeader("X-AnotherHeader", "baz"))
//        .and()
//        .build();
//  }

}